module SupportBot
  module Hooks
    # Reaction Handler
    class ReactionAddHandler < SupportBot::Hooks::Base
      def call(client, data)
        return false unless super

        case data.reaction
        when 'heavy_check_mark', 'white_check_mark'
          reaction_resolve
        else
          logger.info "Unhandled Reaction #{data.reaction}"
        end
      end

      # When a user adds a :heavy_check_mark reaction
      def reaction_resolve
        logger.info 'Reaction Resolve'

        # Find ticket ID and Subject
        ticket = message.text.match(/\[(?<form>.*)\].*#(?<id>-?[0-9]*)>\s-\s(?<subject>.*)/)
        return false unless ticket

        # Ticket update block when resolved by a user / slack reaction
        build_message(
          '',
          [{
            type: 'context',
            elements: [{
              type: 'mrkdwn',
              text: "[#{ticket[:form]}] <#{SupportBot::ZENDESK_LOCATION}/agent/tickets/#{ticket[:id]}|##{ticket[:id]}> - #{ticket[:subject]} - response verified by <@#{username}>"
            }]
          }]
        )
      end
    end
  end
end
