# frozen_string_literal: true

$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'supportbot'

begin
  SupportBot::Bot.run
rescue StandardError => e
  warn "ERROR: #{e}"
  warn e.backtrace
  raise e
end

run SupportBot
