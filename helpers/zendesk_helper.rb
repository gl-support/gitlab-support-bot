# frozen_string_literal: true

require 'mini_cache'
require 'zendesk_api'
require 'json'

module ZendeskHelper
  class ZendeskView
    attr_reader :type, :name, :id

    def initialize(type, name, id)
      @type = type
      @name = name
      @id = id
    end
  end

  def self.count_many
    view_ids = views.keys.map { |v| views[v].id }.join(',')

    is_fresh = false
    while is_fresh == false
      fresh_arr = []
      output_hash = {}
      api_response = zd_client.view_counts(ids: view_ids, path: 'views/count_many', reload: true).fetch
      api_response.each do |json_views|
        fresh_arr.push(json_views['fresh'])
        zendesk_view_key = views.keys.find { |v| views[v].id == json_views['view_id'] }
        output_hash[zendesk_view_key] = json_views['value']
      end
      is_fresh = (fresh_arr.uniq == [true])
    end
    output_hash
  end

  def self.tickets_from_views
    ZendeskHelper.count_many
  end

  def self.views
    return @views unless @views.nil?

    @views = {}
    [
      [:need_org_triage, 'Needs Org & Triage', 360_038_101_960],
      [:dot_com_with_sla, 'GitLab.com with SLA', 360_038_122_959],
      [:sm_with_sla, 'Self-Managed with SLA', 360_038_124_139],
      [:upgrade_renewals_ar, 'License & Renewals', 360_038_103_700]
    ].each do |view_data|
      view = ZendeskView.new(*view_data)
      @views[view.type] = view
    end

    @views
  end

  def self.slah_views
    [
      360_075_980_400, # All new with SLA
      360_075_980_520 # All open with SLA
    ]
  end

  def self.search_for(query, per_page = SupportBot::PER_PAGE)
    query = "#{query} type:ticket status:new status:open status:pending order_by:updated_at sort:desc"
    results = zd_client.search(query: query, per_page: per_page)
    results.to_a
  end

  def self.ticket_support_level(ticket)
    return 'N/A' if ticket.organization.nil?
    return 'N/A' unless ticket.organization.organization_fields[:support_level]

    ticket.organization.organization_fields[:support_level].try(:capitalize)
  end

  def self.slah_query
    logger.info '== Collection ticket information =='
    tickets = slah_tickets

    logger.info "ticket count #{tickets.count}"
    breach_time = Time.now.utc + SupportBot::SLA_BREACH_TIME

    tickets.select do |ticket|
      sla = find_active_slah(ticket)

      next unless sla&.breach_at?

      if Time.at(sla.breach_at) > breach_time
        logger.info "ticket skipped: #{sla.breach_at} > #{breach_time} left, #{ticket.id}"
        next
      end

      true
    end
  end

  def self.only_unreported(tickets)
    report_ticket = []

    tickets.each do |ticket|
      report_ticket.push(ticket) unless cache.set?(ticket.id.to_s)

      cache.set(ticket.id.to_s, expires_in: SupportBot::SLAH_INTERVAL * 2) { ticket }
    end

    save_cache
    report_ticket
  end

  def self.slah_tickets
    tickets = []
    slah_views.each do |view_id|
      tickets << zd_client.views.find!(id: view_id).tickets(include: 'slas').to_a
    end

    tickets.flatten
  end

  def self.find_active_slah(ticket)
    # Zendesk returns a very strange Trackie format looking like this.
    # <ZendeskAPI::Trackie policy_metrics=#<Hashie::Array [
    #   <ZendeskAPI::Trackie breach_at=2020-06-01 03:13:30 UTC days=3 metric="next_reply_time" stage="active">,
    #   <ZendeskAPI::Trackie breach_at=nil metric="first_reply_time" stage="achieved">
    # ]>>
    # return only the SLA that is active (first_reply and next_reply are different)
    ticket.slas['policy_metrics'].find { |sla| sla['stage'] == 'active' }
  end

  def self.logger
    @logger ||= zd_client.config.logger
  end

  def self.cache
    @cache ||= MiniCache::Store.new
  end

  def self.save_cache
    cache_value = []

    @cache.each do |id, value|
      cache_value << { id: id, expires_in: value.expires_in }
    end

    File.open('/tmp/supportbot/cache', 'w') { |file| file.write(Marshal.dump(cache_value)) } unless cache_value.empty?
  end

  def self.load_cache
    cache_value = Marshal.restore File.read('/tmp/supportbot/cache')

    cache_value.each do |ticket|
      cache.set(ticket[:id], expires_in: Time.at(ticket[:expires_in]) - Time.now) { ticket }
    end
    logger.info('Cache loaded')
  rescue Errno::ENOENT
    logger.info('No cache file found')
  end
end
