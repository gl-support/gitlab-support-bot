# frozen_string_literal: true

module EmojiHelper
  def self.get_ticket_emoji(total)
    support_team_size = (ENV['SUPPORT_TEAM_SIZE'] || 100).to_i * 3

    case total
    when 0
      "*#{total}* :yay:"
    when 1..(support_team_size / 7)
      "*#{total}* :fistbump-connect:"
    when (support_team_size / 7)..(support_team_size / 6)
      "*#{total}* :pizza:"
    when (support_team_size / 6)..(support_team_size / 5)
      "*#{total}* :notbad:"
    when (support_team_size / 5)..(support_team_size / 4)
      "*#{total}* :sadpanda:"
    when (support_team_size / 4)..(support_team_size / 3)
      "*#{total}* :panic:"
    when (support_team_size / 3)..(support_team_size / 2)
      "*#{total}* :fire:"
    when (support_team_size / 2)..support_team_size
      "*#{total}* :killitwithfire:"
    else
      "*#{total}* :dumpster-fire:"
    end
  end
end
