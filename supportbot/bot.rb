# frozen_string_literal: true

module SupportBot
  class Bot < SlackRubyBot::Bot
    ABOUT = <<~RESP
      GitLab Slack SupportBot: #{SupportBot::VERSION}
      https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-bot/
      Use the `help` command for more details
    RESP

    def self.run
      # Add hooks to the bot
      instance.on(:reaction_added, SupportBot::Hooks::ReactionAddHandler.new)
      instance.on(:reaction_removed, SupportBot::Hooks::ReactionRemoveHandler.new)

      retry_count = 0
      Thread.new { SupportBot::Commands::SLAH.run_slah_loop } unless ENV['SB_ENVIRONMENT'] == 'dev'

      loop do
        break super
      rescue Slack::Web::Api::Errors::TooManyRequestsError => e
        raise e if retry_count >= SupportBot::MAX_SLACK_RETRIES

        warn "ERROR: #{e}"
        warn "Retrying: #{retry_count}/#{SupportBot::MAX_SLACK_RETRIES}"
        retry_count += 1
        sleep(e.retry_after.seconds)
      end
    end
  end
end
