# frozen_string_literal: true

module SupportBot
  # Slack
  MAX_SLACK_RETRIES = 5

  # Zendesk
  PER_PAGE = 10
  ZENDESK_LOCATION = 'https://gitlab.zendesk.com'

  # SLA
  SLA_BREACH_TIME = 2.hours + 15.minutes
  SLAH_CHANNEL = 'C01AEL6E207' # support_response-crew
  SLAH_INTERVAL = 5 * 60 # seconds

  # Others
  PROJECT_LOCATION = 'gitlab.com/gitlab-com/support/toolbox/gitlab-support-bot'
  VERSION = '0.4.0'
end
