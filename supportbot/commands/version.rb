# frozen_string_literal: true

module SupportBot
  module Commands
    class Version < SlackRubyBot::Commands::Base
      command 'version' do |client, data, _match|
        client.say(channel: data.channel, text: "I am running version: #{SupportBot::VERSION}")
      end
    end
  end
end
