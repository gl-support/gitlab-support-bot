# frozen_string_literal: true

require 'spec_helper'

describe EmojiHelper do
  context 'get_ticket_emoji' do
    it 'should return yay' do
      expect(EmojiHelper.get_ticket_emoji(0)).to eq('*0* :yay:')
    end

    it 'should return fistbump' do
      expect(EmojiHelper.get_ticket_emoji(40)).to eq('*40* :fistbump-connect:')
    end

    it 'should return pizza' do
      expect(EmojiHelper.get_ticket_emoji(49)).to eq('*49* :pizza:')
    end

    it 'should return sad panda' do
      expect(EmojiHelper.get_ticket_emoji(67)).to eq('*67* :sadpanda:')
    end

    it 'should return fire' do
      expect(EmojiHelper.get_ticket_emoji(125)).to eq('*125* :fire:')
    end

    it 'should return kill it with fire' do
      expect(EmojiHelper.get_ticket_emoji(225)).to eq('*225* :killitwithfire:')
    end

    it 'should return dumpster fire' do
      expect(EmojiHelper.get_ticket_emoji(310)).to eq('*310* :dumpster-fire:')
    end
  end
end
