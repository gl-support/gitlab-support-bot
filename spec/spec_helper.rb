# frozen_string_literal: true

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..'))

require 'slack-ruby-bot/rspec'
require 'supportbot'
require 'terminal-table'
require 'pry'

Dir.mkdir('/tmp/supportbot') unless Dir.exist?('/tmp/supportbot')

Dir[File.join(File.dirname(__FILE__), 'support', '**/*.rb')].sort.each do |file|
  require file
end
