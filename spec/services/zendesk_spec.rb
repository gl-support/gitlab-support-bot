# frozen_string_literal: true

require 'spec_helper'

describe 'ZendeskApiClient' do
  subject(:app) { ZendeskApiClient.instance }

  it 'should be of type ZendeskApiClient' do
    expect(app).to be_an_instance_of(ZendeskApiClient)
  end

  it 'should have a config' do
    expect(app.config).to be_an_instance_of(ZendeskAPI::Configuration)
  end
end
