# frozen_string_literal: true

require 'slack-ruby-bot'

# Load services
require 'services/zendesk'

# Load variables before starting everything
require 'supportbot/variables'

# Load helpers
Dir[File.join(__dir__, 'helpers', '*.rb')].each { |file| require file }

# Load hooks
require 'hooks/base'
Dir[File.join(__dir__, 'hooks', '*.rb')].each { |file| require file }

# Load support bot
Dir[File.join(__dir__, 'supportbot', '*.rb')].each { |file| require file }
